import { Component, OnInit } from '@angular/core';
import { Validators} from "@angular/forms";
import { UserService } from '../services/user.service';
import { GlobalService } from '../services/global.service';
import { Router } from '@angular/router';
import { FormGroup, FormBuilder } from '@angular/forms';
import { AuthService } from "angularx-social-login";
import { FacebookLoginProvider, GoogleLoginProvider, LinkedInLoginProvider, SocialUser } from "angularx-social-login";

@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.scss'],
  providers: [UserService]
})
export class RegisterComponent implements OnInit {

  userRegister: FormGroup;
  loading: boolean;

  public user: any = SocialUser;

  constructor(private fb: FormBuilder, private router: Router,
    private userService: UserService, private authService: AuthService) {
    this.userRegister = this.fb.group({
      username: ['', [Validators.required, Validators.email]],
      first_name: ['', [Validators.required, Validators.maxLength(24), Validators.minLength(2)]],
      last_name: ['', [Validators.required, Validators.maxLength(24), Validators.minLength(2)]],
      password: ['', [Validators.required, Validators.minLength(8)]]
    });
  }

  ngOnInit() {
  }

  signInWithGoogle(): void {
    this.authService.signIn(GoogleLoginProvider.PROVIDER_ID).then((userData) => this.user = userData );
  }

  signInWithFB(): void {
    this.authService.signIn(FacebookLoginProvider.PROVIDER_ID);
    this.router.navigate(['/home']);
    console.log(this.user.name);
  }

  signOut(): void {
   this.authService.signOut();
 }


  onRegister() {
    this.loading = true;
    this.userService.registerUser(this.userRegister.value).subscribe(
      response => {
        this.loading = false;

        this.router.navigate(['/login']);
      },
      error => {
        this.loading = false;
        console.log('error', error);
      }
    );
  }


}
