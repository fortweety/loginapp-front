import { Component, OnInit } from '@angular/core';
import { User } from '../models/user';
import { Subscription } from 'rxjs';
import { GlobalService } from '../services/global.service';
import { Router } from '@angular/router';
import { SocialUser } from "angularx-social-login";


@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.scss']
})
export class HomeComponent implements OnInit {

  account: User = new User();
  userSub: Subscription;
  cosial: SocialUser = new SocialUser();

  constructor(private global: GlobalService, private router: Router) { }

  ngOnInit() {
    this.userSub = this.global.user.subscribe(
      me => this.account = me
    );
  }

  logoutClicked() {
   this.global.me = new User();
   localStorage.removeItem('token');
   localStorage.removeItem('account');
   this.router.navigate(['/login']);
 }

}
